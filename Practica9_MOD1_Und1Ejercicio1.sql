﻿USE ciclistas;


/** CONSULTA 1 edad ciclistas 
  **/

SELECT DISTINCT
  c.nombre, c.edad
FROM ciclista c;

/** consulta2 edad, equipo Artiach
  **/

SELECT 
  c.nombre,c.edad, c.nomequipo
  FROM ciclista c
WHERE
c.nomequipo='Artiach';


/** listar edades de los ciclistas de Artiach o Amore Vita
  **/

  SELECT DISTINCT
  c.edad, c.nombre, c.nomequipo
    FROM ciclista c
    WHERE
    c.nomequipo='Artiach'
    OR c.nomequipo='Amore Vita';

/** consulta 4 los dorsales cuya edad <25 o >30
  **/

  SELECT c.dorsal
  FROM 
  ciclista c
    WHERE
    c.edad<25 OR c.edad>30;


/** consulta 5 Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y además que solo sean
de Banesto. **/

SELECT c.dorsal 
FROM ciclista c
  WHERE
   c.edad BETWEEN 28 AND 32
  AND c.nomequipo='Banesto';

/**consulta 6 Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor
que 8 **/

  SELECT 
    c.nombre
  FROM ciclista c
  WHERE
  CHAR_LENGTH(c.nombre)>8;


/** consulta 7 Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo
denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas.
  **/

SELECT 
  c.nombre, c.dorsal, UPPER(nombre) AS `nombre Mayusculas`
FROM ciclista c;


/** consulta 8 Listar todos los ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa.
**/

SELECT DISTINCT
l.dorsal 
FROM lleva l
  WHERE 
  l.código='MGE';




/** consulta 9 Listar el nombre de los puertos cuya 
  altura sea mayor que 1500
**/

SELECT 
  p.nompuerto 
  FROM puerto p
  WHERE
  p.altura >1500; 



 /**consulta 10 Listar el dorsal de los ciclistas que hayan 
  ganado algun puerto cuya pendiente sea
mayor que 8 o cuya altura este entre 1800 y 3000
**/

  SELECT DISTINCT 
    p.dorsal
    FROM puerto p
    WHERE
    p.altura BETWEEN 1800 AND 3000
    OR p.pendiente>8;


/** CONSULTA 11 Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea
mayor que 8 y cuya altura este entre 1800 y 3000
  **/

SELECT DISTINCT
   p.dorsal
   FROM puerto p
  WHERE
  p.altura BETWEEN 1800 AND 3000
  AND
  p.pendiente>8;




   








 